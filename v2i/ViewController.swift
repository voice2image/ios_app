//
//  ViewController.swift
//  v2i
//
//  Created by Ashish Jha on 1/27/18.
//  Copyright © 2018 Ashish Jha. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var imageTextField: UITextField!
    @IBOutlet weak var appLabel: UILabel!
    @IBOutlet weak var generatedImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        imageTextField.delegate = self
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Hide the keyboard.
        textField.resignFirstResponder()
        return true
    }
    
    func addPercentageToUrl(urlString : String) -> String{
        
        return urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)!
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        // appLabel.text = textField.text
        let session = URLSession.shared
        var url_string = "http://ec2-52-213-176-45.eu-west-1.compute.amazonaws.com:8089/text/" + textField.text!
        url_string = addPercentageToUrl(urlString: url_string)
        print(url_string)
        let url = URL(string: url_string)!
        DispatchQueue.global(qos: .userInitiated).async {
        let task = session.dataTask(with: url) { (data, _, _) -> Void in
            if let data = data {
//                let string = String(data: data, encoding: String.Encoding.utf8)
//                print(string) //JSONSerialization
                DispatchQueue.main.async {
                    let image = UIImage(data : data)
                self.generatedImage.image = image
                // self.generatedImage.sizeToFit()
                }
            }
        }
        task.resume()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func getImageButton(_ sender: UIButton) {
        appLabel.text = "BLA BLA"
    }

}

